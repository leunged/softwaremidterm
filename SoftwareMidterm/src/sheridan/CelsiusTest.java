package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class CelsiusTest {

	@Test
	public void testRegular() {
		int celsius = Celsius.fromFahrenheit(50);
		assertTrue("Invalid number of fahrenheit", celsius == 10);
	}
	
	@Test
	public void testExceptional() {
		int celsius = Celsius.fromFahrenheit(-50);
		assertFalse("Invalid number of fahrenheit", celsius == 10);
	}
	
	@Test
	public void testBoundaryIn() {
		int celsius = Celsius.fromFahrenheit(1);
		System.out.println(celsius);
		assertTrue("Invalid number of fahrenheit", celsius == -17);
	}
	
	@Test
	public void testBoundaryOut() {
		int celsius = Celsius.fromFahrenheit(2);
		assertFalse("Invalid number of fahrenheit", celsius == -17);
	}

}
